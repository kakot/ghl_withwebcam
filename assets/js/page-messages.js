


/* 
    Created on  : Nov 29, 2016, 12:50:39 AM
    Author      : Prakasam Mathaiyan
    URL         : http://www.prakasam.co
    E-Mail      : hello@prakasam.co
*/


var Compose=function(){var a=function(){var a=$(".right-message-grid").height();$(".menu-grid").height(a)},b=function(){$(".toUserEmail").chosen({width:"100%",html_template:'{text} <img class="{class_name}" src="{url}">'})},c=function(){"use strict";Dropzone.options.uplMngPrss={paramName:"file",maxFilesize:128,acceptedFiles:".xlsx,.xls,.numbers, .ods, .psd, s.png, .jpeg, .jpg, .rar, .zip, .doc, .docx, .ppt, .pptx",addRemoveLinks:!0}};return{init:function(){a(),b(),c()}}}();