<?php
session_start();

if (!isset($_SESSION['token'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: ../reg_login/login.php');
    exit();
}

if (isset($_GET['logout'])) {
    session_destroy();
    session_unset();
    unset($_SESSION['email']);
    unset($_SESSION['token']);
    unset($_SESSION['logo']);
    unset($_SESSION['institution']);
    unset($_SESSION['success']);
    header("location: ../reg_login/login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>GHL Bank</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Prakasam Mathaiyan">
    <meta name="description" content="">
    <!--[if IE]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../assets/plugins/lib/modernizr.js"></script>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/style-default.css">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/tagsinput/bootstrap-tagsinput.css">
    <style>
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

    </style>
</head>

<body>
<header class="navbar navbar-light bg-light">

    <!-- Image and text -->
    <a class="navbar-brand" href="#">
        <img src="<?php echo $_SESSION['logo']; ?>" width="100" height="30"
             class="d-inline-block align-top" alt="">
        <?php echo $_SESSION['institution']; ?>
    </a>

    <span class="navbar-text">
        <a class="btn btn-danger" href="addclient.php?logout">Log Out</a>
    </span>

</header>
<div class="wrapper has-footer">
    <!--    <header class="header-top navbar fixed-top" style="background-color:#ffffff">
        <div class="top-bar">
            <!-- START: Responsive Search
            <div class="container">
                <div class="main-search">
                    <div class="input-wrap">

                        <a href="#"><i class="sli-magnifier"></i></a>
                    </div>
                    <span class="close-search search-toggle"><i class="ti-close"></i></span>
                </div>
            </div>
        </div>

        <div class="collapse navbar-collapse" id="headerNavbarCollapse">
            <ul class="nav navbar-nav mr-auto">
                <li class="hidden-xs" style="padding-top: 7px">
                    <img src="<?php /*echo $_SESSION['logo'];*/ ?>" width="180">
                </li>

            </ul>
        </div>
    </header>
-->    <!-- END: Header -->

    <div class="main-container">
        <!-- START: Main Container -->

        <div class="page-header">

        </div>

        <div class="content-wrap view-inbox">
            <!--START: Content Wrap-->
            <div class="row">
                <div class="col-md-3">
                    <div class="input-group">
                        <input id="pac-input" type="text" class="form-control"
                               placeholder="Search Location (Place)...">
                        <span class="input-group-btn"></span>
                    </div>
                    <!-- /input-group -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div id="map" style="height: 800px;"></div>
                        </div>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-12" id="item-info">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">CLIENT LOCATOR</h3>
                                <div class="tools">
                                    <a class="btn-link collapses panel-collapse"
                                       href="javascript:;"></a>
                                    <a class="btn-link reload" href="javascript:;"><i
                                                class="ti-reload"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" enctype="multipart/form-data"
                                      action="preview.php" method="post">
                                    <div class="form-group row">
                                        <label for="inputUserName" class="col-sm-4 control-label">Account
                                            #</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="acctno" class="form-control"
                                                   id="inputUserName" required="required"
                                                   placeholder="Account Number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Account
                                            Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="acctname" class="form-control"
                                                   id="acctname" placeholder="Account Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Account
                                            Type</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="accttype" class="form-control"
                                                   id="accttype" placeholder="Account Type">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Date
                                            of Birth</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="dob" class="form-control"
                                                   id="dob" placeholder="Date of Birth">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Occupation</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="occup" class="form-control"
                                                   id="occup" placeholder="Occupation">
                                        </div>
                                    </div>
                                    <div class="form-group row row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Postal
                                            Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="padd" class="form-control"
                                                   id="PAddress" placeholder="Postal Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Street
                                            Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="sadd" class="form-control"
                                                   id="SAddress" placeholder="Street Address">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Latitude
                                            / Longitude</label>
                                        <div class="col-sm-4">
                                            <input type="text" name="lat" class="form-control"
                                                   id="Latitude" placeholder="Latitude">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="long" class="form-control"
                                                   id="Longitude" placeholder="Longitude">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Surburb
                                            / District</label>
                                        <div class="col-sm-4">
                                            <input type="text" name="surb" class="form-control"
                                                   id="Surburb" placeholder="Surburb">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="dist" class="form-control"
                                                   id="City" placeholder="City">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Region</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="reg" class="form-control"
                                                   id="Region" placeholder="Region">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Image
                                            Upload</label>
                                        <div class="col-sm-8">
                                            <input name="userpix" type="file" accept="image/*">
                                            <input id="userpic" name="userpic" type="hidden">


                                        </div>

                                    </div>

                                    <div class="row">
                                        <video class="col-sm-6" autoplay id="video" width="150"
                                               height="150"></video>

                                        <canvas class="col-sm-6" style="visibility: hidden;"
                                                id="canvas"
                                                width="150" height="150"></canvas>
                                        <!--
                                    <input name="userpix" type="file"><br/> <button onclick="snapshot();">Take Snapshot</button>
                                     <p>
                                    Screenshots : <p><canvas  id="myCanvas" width="400" height="350"></canvas>
    -->
                                    </div>

                                    <img class="rounded-circle img-fluid" width="150" height="150"
                                         id="photo"
                                    >

                                    <div class="row" style="padding: 10px;">
                                        <button type="reset"
                                                class="col-sm-4 btn btn-default btn-danger">Reset
                                        </button>

                                        <a class="col-sm-4 btn btn-default btn-info" id="capture">take
                                            photo</a>

                                        <button type="submit"
                                                class="col-sm-4 btn btn-default btn-success">
                                            Preview
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!--END: Content Wrap-->

    </div>
    <!-- END: Main Container -->

    <footer class="footer">
        <!-- START: Footer -->
        &copy; 2017 <b><?php echo $_SESSION['institution']?>. All rights reserved.</b>
    </footer>
    <!-- END: Footer -->

</div>
<!-- END: wrapper -->


<script type="text/javascript" src="../assets/plugins/lib/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="../assets/plugins/lib/jquery-ui.min.js"></script>
<script type="text/javascript" src="../assets/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/plugins/lib/plugins.js"></script>
<script type="text/javascript" src="../assets/plugins/lib/jquery.googlemap.js"></script>
<script type="text/javascript" src="../assets/js/app.base.js"></script>
<script type="application/javascript" src="imagecapture.js"></script>

</body>


</html>


<script>
    var map;
    var markers = [];


    function initMap() {
        var Ghana = {
            lat: 5.57678,
            lng: -0.27416
        };
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                //alert(pos);
                // console.log('You clicked on: ' + pos);

                $("#Latitude").val(pos.lat());
                $("#Longitude").val(pos.lng());
                // var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
                if (responses && responses.length > 0) {

                    $.each(responses[0].address_components, function (i, address_component) {
                        var streetadd = responses[0].formatted_address;
                        var newsadd = streetadd.split(",");
                        //console.log('You clicked on: ' + responses[0].address_components.types[0]); 

                        $("#SAddress").val(newsadd[0]);

                        if (address_component.types[0] == "locality") {
                            console.log(i + ": Third:" + address_component.long_name);
                            $("#City").val(address_component.long_name);
                        }
                        if (address_component.types[0] == "administrative_area_level_2") {
                            console.log(i + ": Surburb :" + address_component.long_name);
                            $("#Surburb").val(address_component.long_name);
                        }

                        if (address_component.types[0] == "administrative_area_level_1") {
                            console.log("Region:" + address_component.long_name);
                            $("#Region").val(address_component.long_name);
                        }
                        if (address_component.types[0] == "post_box") {
                            console.log("PAddress:" + address_component.long_name);
                            $("#PAddress").val(address_component.long_name);
                        }
                    });
                    //   updateMarkerAddress(responses[0].formatted_address);
                } else {
                    //  updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: Ghana,
            mapTypeId: google.maps.MapTypeId.HYBRID //'HYBRID'
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function (event) {
            clearMarkers();
            addMarker(event.latLng);
            geocodePosition(event.latLng);

        });

        // Adds a marker at the center of the map.
        //addMarker(Ghana);
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers2 = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers2.forEach(function (marker) {
                marker.setMap(null);
            });
            markers2 = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers2.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));


                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });


    }


    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: false
        });
        markers.push(marker);
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBkV4Yn4LxCaVECMWyMFi-robcivGseiI&libraries=places&callback=initMap">


</script>

<!--
<script>
      //---------------------
      // TAKE A SNAPSHOT CODE
      //---------------------
      var canvas, ctx;

      function init() {
        // Get the canvas and obtain a context for
        // drawing in it
        canvas = document.getElementById("myCanvas");
        ctx = canvas.getContext('2d');
      }

      function snapshot() {
         // Draws current image from the video element into the canvas
        ctx.drawImage(video, 0,0, canvas.width, canvas.height);
      }

    
</script>
-->
