(function () {
    let video = document.getElementById('video'),
        canvas = document.getElementById('canvas'),
        context = canvas.getContext('2d'),
        photo = document.getElementById('photo'),
        vendorUrl = window.URL || window.webkitURL,
        localstream, capturebtn = document.getElementById('capture'),
        inputImagePath = document.getElementById('userpic');

    navigator.getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

    navigator.getUserMedia({
        video: true,
        audio: false
    }, function (stream) {
        video.src = vendorUrl.createObjectURL(stream);
        localstream = stream;
        // video.play();
    }, function (error) {
        alert(error);
    });


    document.getElementById('capture').addEventListener('click', function () {
        context.drawImage(video, 0, 0, 150, 150);
        const data = canvas.toDataURL('image/jpg');
        photo.setAttribute('src', data);
        inputImagePath.setAttribute('value', data);
        canvas.setAttribute('width', '0');
        canvas.setAttribute('height', '0');


        video.setAttribute('width', '0');
        video.setAttribute('height', '0');
        video.pause();
        video.src = "";
        localstream.getTracks()[0].stop();

        // document.removeChild(capturebtn);
        capturebtn.style.visibility = 'hidden';
    });

})();