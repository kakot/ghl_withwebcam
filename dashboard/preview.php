<?php
$acctno = $_POST['acctno'];
$acctname = $_POST['acctname'];
$accttype = $_POST['accttype'];
$dob = $_POST['dob'];
$occup = $_POST['occup'];
$padd = $_POST['padd'];
$sadd = $_POST['sadd'];
$lat = $_POST['lat'];
$long = $_POST['long'];
$surb = $_POST['surb'];
$dist = $_POST['dist'];
$reg = $_POST['reg'];


$userpix = '';
$image = '';
// get the image to be previewd
$image_path = 'images/' . $acctname . '.png';
if ($_FILES['userpix']['tmp_name'] != '') {
    $aExtraInfo = getimagesize($_FILES['userpix']['tmp_name']);
    $image = "data:" . $aExtraInfo["mime"] . ";base64," . base64_encode(file_get_contents($_FILES['userpix']['tmp_name']));

    $image_location = 'images/';
    $info = pathinfo($_FILES['userpix']['name']);

    // get the extension of the file
    $ext = $info['extension'];
    // the new name of the file
    $newname = $acctname . '.' . $ext;

    $target_file = $image_location . $newname;

    // we store image_path in the db so we can reference the file later
    $image_path = $target_file;

    move_uploaded_file($_FILES['userpix']['tmp_name'], $target_file);
    $_FILES['userpix'] = null;

} else {
    $userpix = $_POST['userpic'];
    $image = $_POST['userpic'];

    $userpix = str_replace('data:image/png;base64,', '', $userpix);
    $userpix = str_replace(' ', '+', $userpix);
    $data = base64_decode($userpix);
    $success = file_put_contents($image_path, $data);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>GHL Bank</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Prakasam Mathaiyan">
    <meta name="description" content="">

    <script type="text/javascript" src="../assets/plugins/lib/modernizr.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/style-default.css">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/tagsinput/bootstrap-tagsinput.css">
</head>

<body>
<div class="wrapper has-footer">
    <header class="header-top navbar fixed-top"
            style="background-color:#ffffff: height: 80px !important">
        <div class="top-bar">
            START: Responsive Search
            <div class="container">
                <div class="main-search">
                    <div class="input-wrap">

                        <a href="#"><i class="sli-magnifier"></i></a>
                    </div>
                    <span class="close-search search-toggle"><i class="ti-close"></i></span>
                </div>
            </div>
        </div>

        <div class="collapse navbar-collapse" id="headerNavbarCollapse">
            <ul class="nav navbar-nav">
                <li class="hidden-xs" style="padding-top: 7px">
                    <img src="../assets/images/logo.jpg" width="150">
                </li>

            </ul>

        </div>
    </header>

    <div class="main-container">


        <div class="content-wrap view-inbox">
            <!--START: Content Wrap-->
            <div class="row">
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div id="map" style="height: 800px;"></div>
                        </div>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-12" id="item-info">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">TRANSACTION SUMMARY</h3>
                                <div class="tools">
                                    <a class="btn-link collapses panel-collapse"
                                       href="javascript:;"></a>
                                    <a class="btn-link reload" href="javascript:;"><i
                                                class="ti-reload"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">

                                <form class="form-horizontal" enctype="multipart/form-data"
                                      action="submit.php" method="post">
                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Account
                                            #</label>
                                        <div class="col-sm-8">
                                            <?php echo $acctno; ?>
                                            <input type="hidden" name="acctno" class="form-control"
                                                   id="inputUserName" required="required"
                                                   placeholder="Account Number"
                                                   value="<?php echo $acctno ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Account
                                            Name</label>
                                        <div class="col-sm-8">
                                            <?php echo $acctname; ?>
                                            <input type="hidden" name="acctname"
                                                   class="form-control" id="acctname"
                                                   placeholder="Account Name"
                                                   value="<?php echo $acctname; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Account
                                            Type</label>
                                        <div class="col-sm-8">
                                            <?php echo $accttype; ?>
                                            <input type="hidden" name="accttype"
                                                   class="form-control" id="accttype"
                                                   placeholder="Account Type"
                                                   value="<?php echo $accttype ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Date
                                            of Birth</label>
                                        <div class="col-sm-8">
                                            <?php echo $dob; ?>
                                            <input type="hidden" name="dob" class="form-control"
                                                   id="dob" placeholder="Account Name"
                                                   value="<?php echo $dob; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Occupation</label>
                                        <div class="col-sm-8">
                                            <?php echo $occup; ?>
                                            <input type="hidden" name="occup" class="form-control"
                                                   id="occup" placeholder="Account Name"
                                                   value="<?php echo $occup; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Postal
                                            Address</label>
                                        <div class="col-sm-8">
                                            <?php echo $padd; ?>
                                            <input type="hidden" name="padd" class="form-control"
                                                   id="padd" placeholder="Account Name"
                                                   value="<?php echo $padd; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Street
                                            Address</label>
                                        <div class="col-sm-8">
                                            <?php echo $sadd; ?>
                                            <input type="hidden" name="sadd" class="form-control"
                                                   id="sadd" placeholder="Account Name"
                                                   value="<?php echo $sadd; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Latitude
                                            / Longitude</label>
                                        <div class="col-sm-4">
                                            <input type="hidden" name="lat" class="form-control"
                                                   id="lat" value="<?php echo $lat; ?> ">
                                            <?php echo $lat; ?> &nbsp;&nbsp;&nbsp;&nbsp; |
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="hidden" name="long" class="form-control"
                                                   id="long" value="<?php echo $long; ?> ">
                                            <?php echo $long; ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Surburb
                                            / District</label>
                                        <div class="col-sm-4">
                                            <input type="hidden" name="surb" class="form-control"
                                                   id="Surburb" placeholder="Account Name"
                                                   value="<?php echo $surb; ?>">
                                            <?php echo $surb; ?> &nbsp;&nbsp;&nbsp;&nbsp; |
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="hidden" name="dist" class="form-control"
                                                   id="city" placeholder="Account Name"
                                                   value="<?php echo $dist; ?>">
                                            <?php echo $dist; ?>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Region</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" name="reg" class="form-control"
                                                   id="Region" placeholder="Account Name"
                                                   value="<?php echo $reg; ?>">
                                            <?php echo $reg; ?>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Image</label>
                                        <div class="col-sm-8">
                                            <img src="<?php echo $image; ?>" width="100">

                                            <input type="hidden" name="userpix" class="form-control"
                                                   placeholder="Account Name"
                                                   value="<?php echo $image_path; ?>">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-sm-4 control-label"></label>
                                        <div class="col-sm-4">
                                            <a href="addclient.php" type="button"
                                               class="btn btn-default btn-warning">Back</a>
                                        </div>

                                        <div class="col-sm-4">
                                            <button type="submit"
                                                    class="btn btn-default btn-success"> Submit
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!--END: Content Wrap-->

        </div>
        <!-- END: Main Container -->

        <footer class="footer">
            <!-- START: Footer -->
            &copy; 2017 <b>GHL Bank Ltd. All rights reserved.</b>
        </footer>
        <!-- END: Footer -->

    </div>
    <!-- END: wrapper -->

    <!--  <script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBkV4Yn4LxCaVECMWyMFi-robcivGseiI&callback=initMap">
</script>-->
    <script type="text/javascript" src="../assets/plugins/lib/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/lib/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/lib/plugins.js"></script>
    <script type="text/javascript" src="../assets/plugins/lib/jquery.googlemap.js"></script>
    <script type="text/javascript"
            src="../assets/plugins/tagsinput/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/typeahead/bloodhound.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/typeahead/typeahead.bundle.min.js"></script>

    <script type="text/javascript" src="../assets/js/app.base.js"></script>
    <script type="text/javascript" src="../assets/js/cmp-taginput.js"></script>
</body>


</html>


<script>
    var map;
    var markers = [];

    function initMap() {
        var latt = $('#lat').val();

        var long = $('#long').val();

        var Ghana = {
            lat: parseFloat(latt),
            lng: parseFloat(long)
        };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: Ghana,
            mapTypeId: 'hybrid'
        });

        var marker = new google.maps.Marker({
            position: Ghana,
            map: map
        });
        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function (event) {
            addMarker(event.latLng);
        });

        // Adds a marker at the center of the map.
        //addMarker(Ghana);


    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markers.push(marker);
    }

    function my_click() {
        var myLatlng = new Array();


        var mapOptions = {
            zoom: 9,
            center: myLatlng[0],
        }

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);


        for (var i = 0; i < myLatlng.length; i++) {
            var marker = new google.maps.Marker({
                position: myLatlng[i],
                title: "sdfdsgfdsfds" + i
            });
            // $("#item-info").show();
            marker.setMap(map);
        }

    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBkV4Yn4LxCaVECMWyMFi-robcivGseiI&libraries=places&callback=initMap">
</script>