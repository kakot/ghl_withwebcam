<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>GHL Bank</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Prakasam Mathaiyan">
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="../assets/plugins/bootstrap/bootstrap.css">
</head>

<body>
<div class="row">
    <?php
    error_reporting(E_ALL);


    $acctno = urlencode($_POST['acctno']);
    $acctname = urlencode($_POST['acctname']);
    $accttype = urlencode($_POST['accttype']);
    $dob = urlencode($_POST['dob']);
    $occup = urlencode($_POST['occup']);
    $padd = urlencode($_POST['padd']);
    $sadd = urlencode($_POST['sadd']);
    $lat = urlencode($_POST['lat']);
    $long = urlencode($_POST['long']);
    $surb = urlencode($_POST['surb']);
    $dist = urlencode($_POST['dist']);
    $reg = urlencode($_POST['reg']);
    $userpix = urlencode($_POST['userpix']);


    $data = 'account_number=' . $acctno . '&account_name=' . $acctname . '&account_type=' . $accttype
        . '&date_of_birth=' . $dob . '&occupation=' . $occup . '&postal_address=' . $padd . '&street_address='
        . $sadd . '&latitude=' . $lat . '&longitude=' . $long . '&suburb=' . $surb . '&district=' . $dist . '&image_url='
        . $userpix . '&region=' . $reg;

    //echo $data;

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://localhost:8000/api/createtransactionsummary",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoxLCJpYXQiOjE1MzEzODczMDV9.k_MnR7aBakG-WnDIREXdlkxpFL8bnJ5GhE5Okt8i9Rs"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    $res = json_decode($response, TRUE);

    if ($err) {
        ?>
        <div class="col-sm-8 col-sm-offset-2 alert alert-danger" role="alert">
            <h2><?php echo $err ?></h2>
        </div>
        <?php
    } else {
        if ($res['success']) {
            ?>
            <div class="col-sm-8 col-sm-offset-2 alert alert-success" role="alert">
                <h2><?php echo $res['message'] ?></h2>
                <!--<link rel="stylesheet" href="">-->
                <p><a href="addclient.php">Click here to locate some and register some more clients!</a>
                </p>
            </div>
            <?php
        } else {
            ?>
            <div class="col-sm-8 col-sm-offset-2 alert alert-danger" role="alert">
                <h2><?php echo $res['message'] ?></h2>
                <!--<link rel="stylesheet" href="">-->
                <p><a href="addclient.php">Click here to return</a></p>
            </div>
            <?php
        }
    }
    ?>
</div>
</body>
</html>
