<?php
session_start();

if (!isset($_SESSION['token'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: reg_login/login.php');
    exit();
}

if (isset($_GET['logout'])) {
    session_destroy();
    session_unset();
    unset($_SESSION['email']);
    unset($_SESSION['token']);
    unset($_SESSION['logo']);
    unset($_SESSION['institution']);
    unset($_SESSION['success']);
    header("location: reg_login/login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="refresh" content="0;url=pages/index.html">
<title>SB Admin 2</title>
<script language="javascript">
    window.location.href = "dashboard/pages/index.php"
</script>
</head>
<body>
Go to <a href="dashboard/pages/index.php">/pages/index.html</a>
</body>
</html>
