<?php include('server.php') ?>
<!DOCTYPE html>

<html>
<head>
    <title>Client Locator Portal</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
</head>
<body>
<div>
    <div class="row container-fluid py-5">
        <div class="col-md-4 offset-md-8">

            <div class="card">
                <div class="card-header">
                    Sign In
                </div>
                <div class="card-body">
                    <form id="register" method="post" action="login.php">
                        <?php include('errors.php'); ?>

                        <div class="form-group row">
                            <label for="email" class="col-sm-3">Email</label>
                            <input required id="email" class="form-control col-sm-9" placeholder="Enter Company Email"
                                   type="email" name="email"
                                   value="<?php echo $email; ?>">
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-3">Password</label>
                            <input required id="password" type="password" name="password_1"
                                   class="form-control col-sm-9" placeholder="Enter Password">
                        </div>
                    </form>
                </div>

                <div class="card-footer">
                    <button form="register" type="submit" class="btn btn-info" name="login_user"> Sign In
                    </button>
                    <p>
                        Not Registered? <a href="register.php">Register</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<style type="text/css">
    body {
        background: url("../assets/images/b2.jpg") no-repeat;
    }
</style>
</html>