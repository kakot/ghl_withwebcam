<?php include('server.php'); ?>
<!DOCTYPE html>

<html>
<head>
    <title>Registration system PHP and MySQL</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
</head>
<body>
<div>
    <div class="row container-fluid py-5">
        <div class="col-md-5 offset-md-7">
<img src="./d">
            <div class="card">
                <div class="card-header">
                    Register
                </div>
                <div class="card-body">
                    <form id="register" enctype="multipart/form-data" method="post" action="register.php">
                        <?php include('errors.php'); ?>

                        <div class="form-group row">
                            <label for="email" class="col-sm-3">Email</label>
                            <input required id="email" class="form-control col-sm-9" placeholder="Enter Company Email"
                                   type="email" name="email"
                                   value="<?php echo $email; ?>">
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-sm-3">Institution</label>
                            <input required id="username" type="text" name="institution" class="form-control col-sm-9"
                                   placeholder="Enter Institution Name"
                                   value="<?php echo $username; ?>">
                        </div>

                        <div class="form-group row">
                            <label for="logo" class="col-sm-3">Logo</label>
                            <input name="logo" type="file" accept="image/*" class="form-control-file col-sm-9" id="logo">
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-3">Password</label>
                            <input required id="password" type="password" name="password_1"
                                   class="form-control col-sm-9" placeholder="Enter Password">
                        </div>
                        <div class="form-group row">
                            <label for="password2" class="col-sm-3">Confirm</label>
                            <input required id="password2" type="password" name="password_2"
                                   class="form-control col-sm-9" placeholder="Repeat Password">
                        </div>

                    </form>
                </div>

                <div class="card-footer">
                    <button form="register" type="submit" class="btn" name="reg_user">Register
                    </button>
                    <p>
                        Already With Us? <a href="login.php">Sign in</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<style type="text/css">
    body {
        background: url("../assets/images/regloginback.jpg") no-repeat center;
    }
</style>
</html>

