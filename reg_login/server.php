<?php
session_start();

// initializing variables
$username = '';
$email = '';
$errors = array();

// connect to the database

// REGISTER USER
if (isset($_POST['reg_user'])) {
    // receive all input values from the form
    $email = $_POST['email'];
    $institution = $_POST['institution'];
    $password_1 = $_POST['password_1'];
    $password_2 = $_POST['password_2'];

    if ($password_1 != $password_2) {
        array_push($errors, 'The two passwords do not match');
    }
    $logo = '';

    if (isset($_FILES['logo']['tmp_name'])) {
        $image_location = '../dashboard/images/logos/';
        $info = pathinfo($_FILES['logo']['name']);

        // get the extension of the file
        $ext = $info['extension'];
        // the new name of the file
        $newName = $institution . '.' . $ext;

        $target_file = $image_location . $newName;

        // we store image_path in the db so we can reference the file later
        $logo = $target_file;

        if (move_uploaded_file($_FILES['logo']['tmp_name'], $target_file))
        {
            unset($_FILES['logo']);
        }

    }


    $req_email = urlencode($email);
    $req_institution = urlencode($institution);
    $req_logo = urlencode($logo);
    $req_password = urlencode(md5($password_1));


    $data = 'email=' . $req_email . '&institution=' . $req_institution . '&logo=' . $req_logo
        . '&password=' . $req_password;
    //echo $data;

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://locator-api.herokuapp.com/createuser',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            'cache-control: no-cache',
            'content-type: application/x-www-form-urlencoded',
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    if ($err){
        array_push($errors, $err);
    }

    if ($response) {
        $res = json_decode($response, true);
        if (!$res['success']) { // if user exists
            array_push($errors, $res['message']);
        } else {
            // Finally, register user if there are no errors in the form
            if (count($errors) == 0) {
                //encrypt the password before sending via the api
                $password = md5($password_1);
                // TODO(DEVELOPER) call api to register user

                $_SESSION['email'] = $res['email'];
                $_SESSION['token'] = $res['token'];
                $_SESSION['logo'] = $res['logo'];
                $_SESSION['institution'] = $res['institution'];
                $_SESSION['success'] = 'You are now logged in';
                header('location: ../index.php');
            }
        }
    }
}

if (isset($_POST['login_user'])) {

    if (count($errors) == 0) {
        $req_password = md5($_POST['password_1']);
        $req_email = urlencode($_POST['email']);

        $data = 'email=' . $req_email . '&password=' . $req_password;
        //echo $data;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://locator-api.herokuapp.com/authenticate',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'cache-control: no-cache',
                'content-type: application/x-www-form-urlencoded',
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) array_push($errors, $err);

        if ($response) {
            $res = json_decode($response, true);
            if ($res['success']) {
                $_SESSION['email'] = $res['email'];
                $_SESSION['token'] = $res['token'];
                $_SESSION['logo'] = $res['logo'];
                $_SESSION['institution'] = $res['institution'];
                $_SESSION['success'] = 'You are now logged in';
                header('location: ../index.php');
            } else {
                array_push($errors, $res['message']);
            }
        }
    }
}
